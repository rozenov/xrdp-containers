FROM docker.io/debian:buster AS lxde
LABEL Name="xrdp-containers"
LABEL Version="0.1"
LABEL org.opencontainers.image.authors="my@rozenov.com"
COPY ./entrypoint /opt/entrypoint
WORKDIR /tmp
ENV VNC_DESKTOP=lxde
ARG DEBIAN_FRONTEND=noninteractive
RUN apt -qq update \
    && apt -qq install -y lxde-core wget \
    && wget https://sourceforge.net/projects/turbovnc/files/2.2.6/turbovnc_2.2.6_amd64.deb/download -q -O turbovnc-amd64.deb \
    && apt install ./turbovnc-amd64.deb -y \
    && chmod +x /opt/entrypoint \
    && useradd -ms /bin/bash user \
    && apt clean autoclean \
    && apt-get autoremove --yes \
    && rm -f ./turbovnc-amd64.deb
USER user
WORKDIR /home/user
EXPOSE 5901/tcp
ENTRYPOINT [ "/opt/entrypoint" ]
CMD [ "start" ]

FROM docker.io/debian:buster AS xfce4
LABEL Name="xrdp-containers"
LABEL Version="0.1"
LABEL org.opencontainers.image.authors="my@rozenov.com"
COPY ./entrypoint /opt/entrypoint
WORKDIR /tmp
ENV VNC_DESKTOP=xfce4
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update \
    && apt -qq install -y xfce4 wget \
    && wget https://sourceforge.net/projects/turbovnc/files/2.2.6/turbovnc_2.2.6_amd64.deb/download -q -O turbovnc-amd64.deb \
    && apt install ./turbovnc-amd64.deb -y \
    && chmod +x /opt/entrypoint \
    && useradd -ms /bin/bash user \
    && apt clean autoclean \
    && apt-get autoremove --yes \
    && rm -f ./turbovnc-amd64.deb
USER user
WORKDIR /home/user
EXPOSE 5901/tcp
ENTRYPOINT [ "/opt/entrypoint" ]
CMD [ "start" ]
